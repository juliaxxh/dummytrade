package com.conygre.training.tradesimulator.dao;

import java.util.List;

import com.conygre.training.tradesimulator.model.Stocks;
// import com.conygre.training.tradesimulator.model.TradeState;
import com.conygre.training.tradesimulator.model.TradeStatus;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeMongoDao extends MongoRepository<Stocks, ObjectId> {

	public List<Stocks> findByStatus(TradeStatus status);
}